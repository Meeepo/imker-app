import React, { Component } from 'react';
import { Button, InputGroup, FormControl, Row, Col, Tabs, Tab, Modal, Accordion, Card, ListGroup} from 'react-bootstrap'
import { AiOutlinePlusCircle, AiFillDelete } from 'react-icons/ai';
import { get, set } from "idb-keyval";




class Stockcard extends Component {
    state = { show: false,
              id: null,
              date: new Date(),
              ageOfQueens: 0,
              entrys: [],
              strenght: "schwach",
              temperament: "bösartig",
              schwarmControl: "fehlt",
              combFit: "flüchtig",
              foodCount: "",
              comment: "",
              amount: 0,
              honey: [],
              newHoney: 0,
              totalHoney: 0

    }

    handleClose = () => this.setState({ show: false});
    handleShow = () => this.setState({ show: true});
    handleCreate = () => {
        const newEntrys = [...this.state.entrys, {date: new Date(), strenght: this.state.strenght, temperament: this.state.temperament, schwarmControl: this.state.schwarmControl, combFit: this.state.combFit, foodCount: this.state.foodCount, comment: this.state.comment}];
        this.setState({ entrys: newEntrys});
        this.setState({ show: false});
    }
    handleDelete = (index) => {
        const newEntrys = this.state.entrys;
        newEntrys.splice(index, 1);
        this.setState({ entrys: newEntrys});
    }
    handleHoney = async () => {
        const newEntry = [...this.state.honey, {date: new Date(), amount: this.state.newHoney}];
        await this.setState({ honey: newEntry})
        this.setState({ newHoney: 0})
        let amount = 0;
        this.state.honey.forEach((element) => {
            amount += parseFloat(element.amount);
        })
        this.setState({totalHoney: amount})
    }

    render() {
        return (
            <div>
                <Tabs className="mt-3 mb-3 mr-3" defaultActiveKey="stockkarte" id="uncontrolled-tab">
                    <Tab eventKey="stockkarte" title="Stockkarte">
                        <Row>
                            <Col>
                                <InputGroup className="mb-3 mt-3">
                                    <InputGroup.Prepend>
                                        <InputGroup.Text>Alter Königin</InputGroup.Text>
                                    </InputGroup.Prepend>
                                        <FormControl name="ageOfQueens" value={this.state.ageOfQueens} onChange={this.handleInputChange} aria-label="text">
                                    </FormControl>
                                </InputGroup>
                            </Col>
                            <Col>
                                <InputGroup className="mb-3 mt-3">
                                    <InputGroup.Prepend>
                                        <InputGroup.Text>Start Datum</InputGroup.Text>
                                    </InputGroup.Prepend>
                                        <FormControl name="date" value={this.state.date} aria-label="text" readOnly>
                                    </FormControl>
                                </InputGroup>
                            </Col>
                            <Col><Button onClick={this.handleShow} className="mb-3 mt-3 mr-3 float-right"><AiOutlinePlusCircle/></Button></Col>
                        </Row>
                        <Accordion>
                                {this.state.entrys.map((element,index) => {
                                    return (<Card key={index}>
                                                <Card.Header>
                                                        <Accordion.Toggle as={Button} variant="link" eventKey={index.toString()}>
                                                            {new Date(element.date).toDateString()}
                                                        </Accordion.Toggle>
                                                </Card.Header>
                                                <Accordion.Collapse eventKey={index.toString()}>
                                                <Card.Body>
                                                    <Row className="mt-3 mb-3">
                                                        <Col sm>
                                                            Stärke: {element.strenght}
                                                        </Col>
                                                        <Col sm>
                                                            Schwarmtrieb: {element.schwarmControl}
                                                        </Col>
                                                        <Col sm> 
                                                            Wabensitz: {element.combFit}
                                                        </Col>
                                                        <Col sm>
                                                            Temperament: {element.temperament}
                                                        </Col>
                                                        <Col sm>
                                                            Futtermenge: {element.foodCount}
                                                        </Col>
                                                        <Col sm>
                                                            Kommentar: {element.comment}
                                                        </Col>
                                                        <Col sm>
                                                            <Button variant="danger" onClick={() => this.handleDelete(index)}><AiFillDelete/></Button>
                                                        </Col>
                                                    </Row>
                                                </Card.Body>
                                                </Accordion.Collapse>
                                            </Card>)
                                })}
                        </Accordion>
                    </Tab>
                    <Tab eventKey="honig" title="Honig Ertrag">
                    <Row>
                    <Col sm={10}>
                    <InputGroup className="mb-3 mt-3 mr-3">
                        <InputGroup.Prepend>
                            <InputGroup.Text>Menge</InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl name="newHoney" value={this.state.newHoney} onChange={this.handleInputChange} aria-label="newHoney" />
                        <InputGroup.Append>
                            <InputGroup.Text>kg</InputGroup.Text>
                        </InputGroup.Append>
                    </InputGroup>
                    </Col>
                    <Col sm={2}><Button onClick={this.handleHoney} className="mb-3 mt-3 mr-3"><AiOutlinePlusCircle/></Button></Col>
                    </Row>
                    <Row>
                    <ListGroup>
                    <Button variant="primary" size="lg" block>Gesamt Menge: {this.state.totalHoney} kg</Button>
                    {this.state.honey.map((element,index) => {
                        return(<ListGroup.Item key={index}>Datum: {new Date(element.date).toDateString()} Menge: {element.amount} kg</ListGroup.Item>)
                    })}
                    </ListGroup>
                    </Row>
                    </Tab>
                </Tabs>
                <Modal show={this.state.show} onHide={this.handleClose}>
                    <Modal.Header closeButton>
                    <Modal.Title>Eintrag erstellen</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                    <InputGroup className="mb-3">
                        <InputGroup.Prepend>
                            <InputGroup.Text>Stärke</InputGroup.Text>
                            </InputGroup.Prepend>
                            <FormControl name="strenght" value={this.state.strenght} onChange={this.handleInputChange} as="select">
                            <option value="schwach">schwach</option>
                            <option value="normal">normal</option>
                            <option value="stark">stark</option>
                            <option value="sehr stark">sehr stark</option>
                            </FormControl>
                    </InputGroup>
                    <InputGroup className="mb-3">
                        <InputGroup.Prepend>
                            <InputGroup.Text>Schwarmtrieb</InputGroup.Text>
                            </InputGroup.Prepend>
                            <FormControl name="schwarmControl" value={this.state.schwarmControl} onChange={this.handleInputChange} as="select">
                            <option value="fehlt">fehlt</option>
                            <option value="leicht lenkbar">leicht lenkbar</option>
                            <option value="schlecht lenkbar">schlecht lenkbar</option>
                            <option value="stark">stark</option>
                            </FormControl>
                    </InputGroup>
                    <InputGroup className="mb-3">
                        <InputGroup.Prepend>
                            <InputGroup.Text>Wabensitz</InputGroup.Text>
                            </InputGroup.Prepend>
                            <FormControl name="combFit" value={this.state.combFit} onChange={this.handleInputChange} as="select">
                            <option value="flüchtig">flüchtig</option>
                            <option value="laufend">laufend</option>
                            <option value="ruhig">ruhig</option>
                            <option value="fest">fest</option>
                            </FormControl>
                    </InputGroup>
                    <InputGroup className="mb-3">
                        <InputGroup.Prepend>
                            <InputGroup.Text>Temperament</InputGroup.Text>
                            </InputGroup.Prepend>
                            <FormControl name="temperament" value={this.state.temperament} onChange={this.handleInputChange} as="select">
                            <option value="bösartig">bösartig</option>
                            <option value="nervös">nervös</option>
                            <option value="sanft">sanft</option>
                            <option value="sehr sanft">sehr sanft</option>
                            </FormControl>
                    </InputGroup>
                    <InputGroup className="mb-3 mt-3">
                        <InputGroup.Prepend>
                            <InputGroup.Text>Futtermenge</InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl name="foodCount" value={this.state.foodCount} onChange={this.handleInputChange} aria-label="text">
                        </FormControl>
                    </InputGroup>
                    <InputGroup className="mb-3 mt-3">
                        <InputGroup.Prepend>
                            <InputGroup.Text>Kommentar</InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl name="comment" value={this.state.comment} onChange={this.handleInputChange} aria-label="text">
                        </FormControl>
                    </InputGroup>
                    </Modal.Body>
                    <Modal.Footer>
                    <Button variant="secondary" onClick={this.handleClose}>
                        Abbrechen
                    </Button>
                    <Button variant="primary" onClick={this.handleCreate}>
                        Eintrag hinzufügen
                    </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if(nextProps.id != null && nextProps.id !== prevState.id) {
            return {id: nextProps.id}
        } else {
            return {}
        }
    }

    componentDidUpdate() {
        //update db
        if(this.state.date != null && this.state.ageOfQueens != null && this.state.entrys !== []) {
        const stockcardEntry = {id: this.state.id, date: this.state.date, ageOfQueens: this.state.ageOfQueens, entrys: this.state.entrys, honey: this.state.honey}
        set('stockcards_' + this.state.id, JSON.stringify(stockcardEntry));
        }
    }
    componentDidMount() {
        if(this.state.id !== null) {
            get('stockcards_' + this.state.id)
            .then(val => {
                const stockcard = JSON.parse(val)
                this.setState({date: stockcard.date, 
                               ageOfQueens: stockcard.ageOfQueens, 
                               entrys: stockcard.entrys, honey: stockcard.honey})
                let amount = 0;
                stockcard.honey.forEach((element) => {
                    amount += parseFloat(element.amount);
                })
                this.setState({totalHoney: amount})
            }).catch(err => console.log('DB Load failed', err));
        }
        
    }

    handleInputChange = (event) => {
        const target = event.target;
        const value = target.className === 'select' ? target.text : target.value;
        const name = target.name;
        this.setState({
          [name]: value    });
    }
}

export default Stockcard;