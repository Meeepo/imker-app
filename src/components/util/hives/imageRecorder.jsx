import React, {  useEffect, useRef } from "react";
import { ReactMediaRecorder } from "react-media-recorder";
import { Button, Container } from 'react-bootstrap';
import { set, del } from "idb-keyval";
import {NotificationContainer, NotificationManager} from 'react-notifications';

const VideoPreview = ({ stream }) => {
    const videoRef = useRef(null);
  
    useEffect(() => {
      if (videoRef.current && stream) {
        videoRef.current.srcObject = stream;
      }
    }, [stream]);
    if (!stream) {
      return null;
    }
    return <video ref={videoRef} width={"100%"} height={400} autoPlay controls />;
  };

  function ImageRecorder({hiveId, handleClose}) {

    return(
    <Container>
    <NotificationContainer/>
    <ReactMediaRecorder
      video
      render={({ previewStream }) => {
        return (
        <div>
        <VideoPreview stream={previewStream} />
        <img id="imageTag" width={"100%"} height={"100%"} alt=""/>
        <Button className="float-right mt-1" onClick={() => takePhoto(previewStream, hiveId, handleClose)}>Neues Bild speichern</Button>
        <Button className="float-right mt-1 mr-3" variant="danger" onClick={() => removePhoto(hiveId)}>Bild Löschen</Button>
        </div>)
      }}
    />
    </Container>
    )
  };

  function takePhoto(theStream, hiveId, handleClose) {
    if (!('ImageCapture' in window)) {
      alert('ImageCapture is not available');
      return;
    }
    
    if (!theStream) {
      alert('Grab the video stream first!');
      return;
    }
    
    var theImageCapturer = new ImageCapture(theStream.getVideoTracks()[0]);
  
    theImageCapturer.takePhoto()
      .then(blob => {
        set('image_' + hiveId, blob);
        NotificationManager.success('Neues Photo gespeichert')
        handleClose();
        //var theImageTag = document.getElementById("imageTag");
        //theImageTag.src = URL.createObjectURL(blob);
        
      })
      .catch(err => alert('Error: ' + err));
  }

  function removePhoto(hiveId) {
    del('image_' + hiveId);
    NotificationManager.info('Altes Photo gelöscht')
  }

  export default ImageRecorder;