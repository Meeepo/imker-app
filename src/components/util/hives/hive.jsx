import React, { Component } from 'react'
import { Button, InputGroup, FormControl, Container, DropdownButton, Dropdown, Row} from 'react-bootstrap'
import { geolocated, geoPropTypes } from "react-geolocated";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { MdGpsFixed } from 'react-icons/md';
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import Stockcard from './stockcard'
import { set } from "idb-keyval";
import Hives from "../../hives"
import Image from "./image"
//{id: 1, name: "test", sidename: "", position: null, image: null, nfc: null, creationDate: null}

class Hive extends Component {

    getNextId = () => {
        if (this.props.hives.length === 0) {
            return 1;
        } else {
            return this.props.hives[this.props.hives.length-1].id+1;
        }
    }

    state = { 
        id: this.getNextId(),
        creationDate: new Date(),
        name: "",
        sidename: "",
        position: "",
        image: null,
        nfc: null,
        arrayIndex: null,
        sidenameList: this.props.sidenameList,
        selected: null,
        showImage: false
     }

    handleInputChange = (event) => {
        const target = event.target;
        const value = target.className === 'dropdown-item' ? target.text : target.value;
        const name = target.name;
        this.setState({
          [name]: value    });
    }

    componentDidMount() {
        NotificationManager.listNotify.forEach(n => NotificationManager.remove({ id: n.id }));
    }

    handleCreate = () => {
        const entrys = [...this.props.hives, {id: this.state.id, name: this.state.name, sidename: this.state.sidename, position: this.state.position, image: this.state.image, nfc: this.state.nfc, creationDate: this.state.creationDate}]
        this.props.handleHives(entrys);
        this.props.resetView();
        const stockcardEntry = {id: this.state.id, date: this.state.creationDate, ageOfQueens: 0, entrys: [], honey: []}
        set('stockcards_' + this.state.id, JSON.stringify(stockcardEntry));
    }

    handleSave = () => {
        let entrys = this.props.hives
        entrys[this.state.arrayIndex] = {id: this.state.id, name: this.state.name, sidename: this.state.sidename, position: this.state.position, image: this.state.image, nfc: this.state.nfc, creationDate: this.state.creationDate}
        this.props.handleHives(entrys);
        this.props.resetView();
    }

    handlePosition = () => {
        if(this.props.coords != null) {
            this.setState({position: (this.props.coords.latitude + " " + this.props.coords.longitude)})
            NotificationManager.info('Updated Position')
        } else {         
            NotificationManager.error('GPS nicht verfügbar')
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if(nextProps.selected != null && nextProps.selected !== prevState.selected && prevState.arrayIndex === null) {
            const arrayIndex = nextProps.hives.findIndex(p => p.id === nextProps.selected)
            if(arrayIndex !== -1) {
                return {
                    id: nextProps.selected, 
                    creationDate: nextProps.hives[arrayIndex].creationDate,
                    name: nextProps.hives[arrayIndex].name,
                    sidename: nextProps.hives[arrayIndex].sidename,
                    position: nextProps.hives[arrayIndex].position,
                    image: nextProps.hives[arrayIndex].image,
                    nfc: nextProps.hives[arrayIndex].nfc,
                    arrayIndex: arrayIndex,
                 }
            }
        } else {
            return {}
        }
    }

    writeNFC = () => {
        //write fake nfc
        let timeout = (Math.random() * (3 - 1) + 1) * 1000;
        Hives.delay(timeout).then(() => {
            NotificationManager.info('NFC erfolgreich geschrieben')
        });
    }

    handleImage = () => {
        this.setState(prevState => ({
            showImage: !prevState.showImage
          }));
    }

    render() {

        let button, header, details

        if(this.state.arrayIndex != null) {
            header = "Volk bearbeiten"
            button = (<div><Button className="mb-3 mt-3 mr-3" onClick={this.handleSave}>Speichern</Button>
            <Button className="mb-3 mt-3 mr-3" onClick={this.writeNFC}>NFC erstellen</Button>
            <Button className="mb-3 mt-3 mr-3" onClick={this.handleImage}>Bild hinterlegen</Button></div>)
            details = (<Stockcard
                        id={this.state.id}
            />)
        } else {
            header = "Neues Volk erstellen"
            button = <Button className="mb-3 mt-3 mr-3" onClick={this.handleCreate}>Erstellen</Button>
        }
        return ( <div>
            <NotificationContainer/>
            <AppBar position="static" style={{ background: '#db8300' }}>
                <Toolbar>
                    <Typography variant="h6" color="inherit">
                    {header}
                    </Typography>
                </Toolbar>
            </AppBar>
            <Container className="mt-3" fluid>
            <Row>
            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text>Namen</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl name="name" value={this.state.name} onChange={this.handleInputChange} aria-label="text">
                </FormControl>
            </InputGroup>
            <InputGroup className="mb-3">
            <InputGroup.Prepend>
                <InputGroup.Text>Standort</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                aria-label="locations"
                aria-describedby="basic-addon2"
                name="sidename"
                value={this.state.sidename}
                onChange={this.handleInputChange}
                />

                <DropdownButton
                as={InputGroup.Append}
                variant="outline-secondary"
                title="Standort"
                id="input-group-dropdown-2"
                >
                {this.state.sidenameList.map((element,index) => (
                    <Dropdown.Item key={index} name="sidename" onClick={this.handleInputChange} href="#">{element}</Dropdown.Item>
                ))}
                </DropdownButton>
            </InputGroup>
            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text>Position</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl readOnly name="position" value={this.state.position} aria-label="text"></FormControl>
                <InputGroup.Append>
                    <Button onClick={this.handlePosition}><MdGpsFixed/></Button>
                </InputGroup.Append>
            </InputGroup>
            {button}      
            <Button className="mb-3 mt-3 mr-3" onClick={this.props.resetView}>Zurück</Button>
            </Row>
            {details}
            </Container>
            {this.state.showImage && <Image
                        show={true}
                        handleImage={this.handleImage}
                        hiveId={this.state.id}
                />
            }
            
        </div> );
    }
}
 
// Using Object.assign
Hive.propTypes = Object.assign({}, Hive.propTypes, geoPropTypes);
// Using ES6 object spread syntax
Hive.propTypes = { ...Hive.propTypes, ...geoPropTypes };

export default geolocated({
    positionOptions: {
        enableHighAccuracy: true,
    },
    userDecisionTimeout: 5000,
}) (Hive);