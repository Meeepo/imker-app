import React, { Component } from 'react';
import { Button, Modal} from 'react-bootstrap';
import ImageRecorder from './imageRecorder'

class Image extends Component {
    state = { hiveId: -1, show: false}

    handleClose = () => {
        this.props.handleImage();
    }
    handleShow = () => {
        this.setState({show: true});
    }
    handleNewImage = () => {
        this.props.handleImage();
    }

    render() {
        return (
            <div>
                <Modal show={this.state.show} onHide={this.handleClose}>
                    <Modal.Header closeButton>
                    <Modal.Title>Bild hinzufügen</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <ImageRecorder
                        hiveId={this.props.hiveId}
                        handleClose={this.handleClose}
                        />
                    </Modal.Body>
                    <Modal.Footer>
                    <Button variant="secondary" className="mr-4" onClick={this.handleClose}>
                        Zurück
                    </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if(nextProps.show !== prevState.show) {
            return {show: nextProps.show}
        } else {
            return {}
        }
    }
}

export default Image;