import React from "react";
import Leaflet from "leaflet";
import { MapContainer, TileLayer, useMap, Marker, Tooltip, LayersControl, LayerGroup } from "react-leaflet";
import "leaflet/dist/leaflet.css";
import icon from "leaflet/dist/images/marker-icon.png";
import iconShadow from "leaflet/dist/images/marker-shadow.png";
import iconRetina from "leaflet/dist/images/marker-icon-2x.png";
import { useHistory } from "react-router-dom";

let DefaultIcon = Leaflet.icon({
  ...Leaflet.Icon.Default.prototype.options,
  iconUrl: icon,
  iconRetinaUrl: iconRetina,
  shadowUrl: iconShadow
});
Leaflet.Marker.prototype.options.icon = DefaultIcon;

function SetViewOnClick({ coords }) {
  const map = useMap();
  map.setView(coords, map.getZoom());

  return null;
}

function Map({ coords, hives }) {
  const history = useHistory();
  let sidenames = generateSidenameList(hives);
  return (<div>
    <MapContainer
      classsName="map"
      center={coords}
      zoom={13}
      scrollWheelZoom={true}
    >
      <TileLayer
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <SetViewOnClick coords={coords} />
      <LayersControl position="topright">
        {
          sidenames.map((element,index) => {
            return (<LayersControl.Overlay checked key={index} name={element.sidename}><LayerGroup>
              {element.marker.map((element,index) => {
                  if(element.position != null && element.position !== "") {
                  return (<Marker key={index} 
                                  position={element.position.split(" ")}
                                  eventHandlers={{
                                    click: () => {
                                      history.replace({pathname: '/hives',state: { selected: element.id }})
                                    },
                                  }}>
                            <Tooltip>
                            {element.name}
                            </Tooltip>
                          </Marker>)} else {return null}
              })}
            </LayerGroup></LayersControl.Overlay>)
          })}
          {
            hives.map((element,index) => {
              if((element.sidename === "" || element.sidename == null) && element.positon !== "") {
                return (
                <LayersControl.Overlay checked key={index} name={element.name}>
                  <Marker position={element.position.split(" ")} 
                                    eventHandlers={{
                                    click: () => {
                                      history.replace({pathname: '/hives',state: { selected: element.id }})
                                    },
                                  }}>
                    <Tooltip>
                    {element.name}
                    </Tooltip>
                  </Marker>
                </LayersControl.Overlay>)
              } else { return null}
            })
          }
      </LayersControl>
      </MapContainer>
      </div>
  );
}

function generateSidenameList(hives) {
  let sidenameList = []
        hives.forEach(element => {
          if(element.sidename !== "") {
            if(sidenameList.findIndex(p => p.sidename === element.sidename) === -1) {
                sidenameList.push({"sidename": element.sidename, "marker": [element]})
            } else {
              sidenameList[sidenameList.findIndex(p => p.sidename === element.sidename)].marker.push(element)
            }
          }
        });
  return sidenameList
}

export default Map;
