/* eslint-disable import/no-anonymous-default-export */
/**/
/*GET request for getting weather API Data in JSON */

export default async location => {
    try {
        const result = await fetch(
            `https://api.openweathermap.org/data/2.5/weather?q=${location}&appid=a3e4da9fee13d3234352947e7c0bfead&units=metric`,
        );
        /*Check Status to see, if the entered location can be found*/
        if (result.status === 200) {
            return { success: true, data: await result.json() };
        }
        /*Error Message- if call was not successfull*/
        return { success: false, error: result.statusText };
    } catch (ex) {
        return { success: false, error: ex.message };
    }
};
