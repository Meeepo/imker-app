import React from "react";
import PropTypes from "prop-types";
import {
  makeStyles,
  ThemeProvider,
  createMuiTheme,
} from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Avatar from "@material-ui/core/Avatar";
import Tooltip from "@material-ui/core/Tooltip";
import CircularProgress from "@material-ui/core/CircularProgress";
import ArrowRightAltIcon from "@material-ui/icons/ArrowRightAlt";
import ErrorIcon from "@material-ui/icons/Error";
import getLocationWeather from "./getLocationWeather";

const useStyles = makeStyles((theme) => ({
  headerLine: {
    display: "flex",
    alignItems: "center",
  },
  location: {
    flex: 1,
  },
  detailLine: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  description: {
    flex: 1,
  },
  largeAvatar: {
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
}));
const themee = createMuiTheme();

themee.typography.h6 = {
  fontSize: "1rem",

  [themee.breakpoints.between("sm", "md")]: {
    fontSize: "1.2rem",
  },
  [themee.breakpoints.between("md", "lg")]: {
    fontSize: "1.5rem",
  },
  [themee.breakpoints.between("lg", "xl")]: {
    fontSize: "1.6rem",
  },
};

function LoadingIndicator({ isLoading }) {
  return isLoading ? <CircularProgress /> : null;
}

function ErrorMessage({ apiError }) {
  if (!apiError) return null;

  return (
    <>
      <ErrorIcon color="error" />
      <Typography color="error" variant="h6">
        {apiError}
      </Typography>
    </>
  );
}
/*Weather API Data */
function WeatherDisplay({ weatherData }) {
  const classes = useStyles(); /**This useMemo hoock transforms it into a isplayable object */
  const {
    temp,
    description,
    icon,
    windTransform,
    windSpeed,
    humidity,
  } = React.useMemo(() => {
    const [weather] = weatherData.weather || [];
    return {
      temp:
        weatherData.main && weatherData.main.temp
          ? Math.round(weatherData.main.temp).toString()
          : "",
      description: weather ? weather.description : "",
      icon: weather
        ? `https://openweathermap.org/img/wn/${weather.icon}@2x.png`
        : "",
      windTransform: weatherData.wind ? weatherData.wind.deg - 90 : null,
      windSpeed: weatherData.wind ? Math.round(weatherData.wind.speed) : 0,
      humidity:
        weatherData.main && weatherData.main.humidity
          ? Math.round(weatherData.main.humidity).toString()
          : "",
    };
  }, [weatherData]);

  return (
    <>
      <ThemeProvider theme={themee}>
        {<Typography variant="h6">{temp}°C</Typography>}
        {icon && (
          <Tooltip title={description} aria-label={description}>
            <Avatar
              className={classes.largeAvatar}
              alt={description}
              src={icon}
            />
          </Tooltip>
        )}
        {windSpeed > 0 && (
          <>
            {windTransform !== null && (
              <ArrowRightAltIcon
                style={{ transform: `rotateZ(${windTransform}deg)` }}
              />
            )}
            <Typography variant="h6">{`${windSpeed} km/h`}</Typography>
            <ArrowRightAltIcon
              style={{ transform: `rotateZ(${windTransform}deg)` }}
            />
          </>
        )}
        {<Typography variant="h6">{` ${humidity} % RH`}</Typography>}
      </ThemeProvider>
    </>
  );
}
/*Stores 2 states: one for weather data and other for error message*/
function LocationWeather({ location }) {
  const classes = useStyles();

  const [weatherData, setWeatherData] = React.useState({});
  const [apiError, setApiError] = React.useState("");
  const [isLoading, setIsLoading] = React.useState(
    false
  ); /*Loading indicator */

  React.useEffect(() => {
    /*effect hook*/
    const loadingIndicatorTimeout = setTimeout(() => setIsLoading(true), 500);
    const getWeather = async () => {
      /* useEffect can't be an async founction-hence declaring inline function*/
      const result = await getLocationWeather(location);
      clearTimeout(loadingIndicatorTimeout);
      setIsLoading(false);
      setWeatherData(result.success ? result.data : {});
      setApiError(result.success ? "" : result.error);
    }; /*If API call was successful->weather data set to rhe result and error description is empty string and vice versa*/

    getWeather();
    return () => clearTimeout(loadingIndicatorTimeout);
  }, [location]);

  const { flagIcon, countryCode } = React.useMemo(() => {
    return {
      flagIcon: weatherData.sys
        ? `https://www.countryflags.io/${weatherData.sys.country}/shiny/32.png`
        : "",
      countryCode: weatherData.sys ? weatherData.sys.country : "",
    };
  }, [weatherData]);

  return (
    <>
      <div className={classes.headerLine}>
        <Typography className={classes.location} variant="h5">
          {location}
        </Typography>
        {flagIcon && <img alt={countryCode} src={flagIcon} />}
      </div>
      <div className={classes.detailLine}>
        <LoadingIndicator isLoading={isLoading} />
        <ErrorMessage apiError={apiError} />
        <WeatherDisplay weatherData={weatherData} />
      </div>
    </>
  );
}

LocationWeather.propTypes = {
  location: PropTypes.string.isRequired,
};

export default LocationWeather;
