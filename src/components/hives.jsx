import React, { Component } from 'react'
import { Button, Card, Container, Row, Col, InputGroup, FormControl} from 'react-bootstrap'
import default_img from './img/default_img_hives.png';
import Hive from './util/hives/hive';
import { set, get, del } from "idb-keyval";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import {NotificationContainer, NotificationManager} from 'react-notifications';
import { withRouter } from 'react-router-dom';


class Hives extends Component {
    
    state = {
        hives: [],
        view: 0,
        selected: -1,
        sidenameList: [],
        filterSide: "Alle"
    }

    render() { 
        if (this.state.view === 0) {
        return (
            <div>
                <NotificationContainer/>
                <AppBar position="static" style={{ background: '#db8300' }}>
                <Toolbar>
                    <Typography variant="h6" color="inherit">
                        Völker
                    </Typography>
                </Toolbar>
            </AppBar>
            <Container fluid>
            <Row>
            <Col>
                <Button className="mb-3 mt-3" onClick={() => this.setState({view: 1})}>Neues Volk erstellen</Button>
                <Button className="ml-3 mb-3 mt-3" onClick={this.handleNFC}>Read NFC</Button>
            </Col>
            <Col>
                <InputGroup style={{float: "right"}} className="mb-3 mt-3">
                <InputGroup.Prepend>
                    </InputGroup.Prepend>
                    <FormControl name="filterSide" value={this.state.filterSide} onChange={this.handleInputChange} as="select" aria-label="type">
                    <option key="-1" value="Alle">Alle</option>
                    {this.state.sidenameList.map((element,index) => (
                    <option key={index} value={element}>{element}</option>))}
                    </FormControl>
                </InputGroup>
            </Col>
            </Row>
            <Row>
            {this.state.hives.map((element,index) => {
                    if(element.sidename === this.state.filterSide || this.state.filterSide === 'Alle') {
                        return (<Col key={index} md={3} sd={3} xs={6}>
                        <Card className="mb-3 mr-3"  style={{ width: '100%' }}>
                            <Card.Img id={"image" + element.id} variant="top" src={this.loadImage(element.id)} />
                            <Card.Body>
                                <Card.Title>{element.name}</Card.Title>
                                <Card.Text>
                                {element.sidename ? element.sidename : <br/> }
                                </Card.Text>
                                <Button onClick={() => this.setView(element.id)} className="mr-3 mb-3">Edit</Button>
                                <Button onClick={() => this.deleteHive(index)} variant="danger" className="mr-3 mb-3">Delete</Button>
                            </Card.Body>
                        </Card>
                        </Col>)} else {return null}
            })}
            </Row>
            </Container>      
        </div> );
        }
        //new hive
        if (this.state.view === 1) {
            return (
                <Container fluid>
                    <Hive 
                        resetView={this.resetView}
                        handleHives={this.handleHives}
                        hives={this.state.hives}
                        sidenameList={this.state.sidenameList}
                    />
                </Container>
            )
        }
        //details to hive
        if (this.state.view === 2) {
            return (
                <Container fluid>
                    <Hive
                        selected={this.state.selected}
                        resetView={this.resetView}
                        handleHives={this.handleHives}
                        hives={this.state.hives}
                        sidenameList={this.state.sidenameList}
                    />                
                </Container>
            )
        }

    }

    static delay(ms){
        var ctr, rej, p = new Promise(function (resolve, reject) {
            ctr = setTimeout(resolve, ms);
            rej = reject;
        });
        p.cancel = function(){ clearTimeout(ctr); rej(Error("Cancelled"))};
        return p; 
    }

    loadImage(id) {
        let image = default_img
        get('image_' + id)
        .then(val => {
            var theImageTag = document.getElementById("image" + id);
            theImageTag.src = window.URL.createObjectURL(val);
        // eslint-disable-next-line no-unused-vars
        }).catch(err => {const fail = err});
        return image
    }
    

    handleNFC = () => {
        NotificationManager.info('Lesevorgang gestartet')
        //fake nfc read because it is not working on any phone
        //read nfc
        if(false) { //nfc read failed
            NotificationManager.error('Read Tag failed', 'NFC')
        }
        let timeout = (Math.random() * (3 - 1) + 1) * 1000;
        Hives.delay(timeout).then(() => {
        //lookup nfc
        //let obj = this.state.hives.find(o => o.nfc === 'tag data');
        let item = this.state.hives[Math.floor(Math.random() * this.state.hives.length)];
        //set view
        this.setView(item.id)});
    }
    

    handleInputChange = (event) => {
        const target = event.target;
        const value = target.className === 'dropdown-item' ? target.text : target.value;
        const name = target.name;
        this.setState({
          [name]: value    });
    }

    setView = (id) => {
        this.setState({selected: id})
        this.setState({view: 2})
    }

    resetView = () => {
        this.setState({view: 0})
        this.setState({selected: -1})
    }

    deleteHive = async (id) => {
        del('stockcards_' + this.state.hives[id].id);
        del('image_' + this.state.hives[id].id);
        await this.setState({hives: this.state.hives.filter((currentValue, index) => index !== id)});
        this.generateSidenameList(this.state.hives);
    }

    handleHives = async (newHives) => {
        await this.setState({hives: newHives});
        this.generateSidenameList(this.state.hives);
        set('hives', JSON.stringify(this.state.hives));
    }

    componentDidUpdate() {
        //update db
        set('hives', JSON.stringify(this.state.hives));
        NotificationManager.listNotify.forEach(n => NotificationManager.remove({ id: n.id }));
    }

    async componentDidMount() {
        if(this.state.hives.length === 0) {
            await get('hives')
            .then(val => {
                this.setState({hives: JSON.parse(val)})
                this.generateSidenameList(JSON.parse(val))
            }).catch(err => console.log('DB Load failed', err));
        }
        if(typeof(this.props.location.state) != 'undefined') {
            this.setView(this.props.location.state.selected)
        }
    }

    generateSidenameList = (hives) => {
        //regenerate sidenameList
        let sidenameList = []
        hives.forEach(element => {
            if(sidenameList.indexOf(element.sidename) === -1 && element.sidename !== "") {
                sidenameList.push(element.sidename)
            }
        });
        this.setState({sidenameList: sidenameList})
    }   
    
}
 
export default withRouter(Hives);