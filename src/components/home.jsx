import React, { Component } from 'react';
import { get, set } from "idb-keyval";
import { Alert, Container, Row, Col, Button, ListGroup, InputGroup, FormControl } from 'react-bootstrap';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { AiOutlinePlusCircle, AiFillDelete } from 'react-icons/ai';
import { BiMinus } from 'react-icons/bi';
import { BsPlus } from 'react-icons/bs';
import { GiBeehive } from 'react-icons/gi';



class Home extends Component {
    state = { hives: [],
        lastEntrys: [],
        balance: 0,
        aufgabe: "",
        aufgaben: []
    }
    render() { 
        return ( <div>
            <AppBar position="static" style={{ background: '#db8300' }}>
                <Toolbar>
                    <Typography variant="h6" color="inherit">
                        Dashboard
                    </Typography>
                </Toolbar>
            </AppBar>
            <Container fluid>
                <Row className="mt-3 mb-3">
                    <Col>
            <Alert className="mt-3 mr-3 ml-3" variant="dark"><h3><Row className="mt-3 mb-3">Anzahl Völker: {this.state.hives.length}<GiBeehive/></Row></h3></Alert>
            </Col>
            </Row>
            <Row className="mt-3 mb-3">
            <Col>
            <Alert className="mt-3 mr-3 ml-3" variant="dark">
            <h3>Letzte Buchungen im Kassenbuch:</h3>
            <ListGroup>
                    {this.state.lastEntrys.map((element,index) => {
                        return(<ListGroup.Item key={index}>{element.type === "Ausgaben" && <BiMinus/>}{element.type === "Einnahmen" && <BsPlus/>} Buchungstext: {element.text} Buchungswert: {element.amount}</ListGroup.Item>)
                    })}
                    <Button variant="primary" size="lg" block>Saldo: {this.state.balance}</Button>
            </ListGroup>
            </Alert>
            </Col>
            </Row>
            <Row className="mt-3 mb-3">
                <Col>
                    <Alert className="mt-3 mr-3 ml-3" variant="dark">
                    <h3>Aufgaben</h3>
                    <InputGroup className="mb-3 mt-3">
                        <InputGroup.Prepend>
                            <InputGroup.Text>Aufgabe:</InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl name="aufgabe" value={this.state.aufgabe} onChange={this.handleInputChange} aria-label="text">
                        </FormControl>
                        <InputGroup.Append>
                        <Button onClick={this.handleAdd}><AiOutlinePlusCircle/></Button>
                        </InputGroup.Append>
                    </InputGroup>
                    <ListGroup>
                    {this.state.aufgaben.map((element,index) => {
                        return(<ListGroup.Item key={index}>{index+1}: {element}<Button variant="danger" className=" mr-3 float-right" onClick={() => this.handleDelete(index)}><AiFillDelete/></Button></ListGroup.Item>)
                        })}
                    </ListGroup>
                    </Alert>
                </Col>
            </Row>
            </Container>

        </div> );
    }

    handleAdd = () => {
        const newEntrys = [...this.state.aufgaben, this.state.aufgabe];
        this.setState({ aufgaben: newEntrys});
    }

    handleDelete = (index) => {
        const newEntrys = this.state.aufgaben;
        newEntrys.splice(index, 1);
        this.setState({ aufgaben: newEntrys});
    }

    componentDidMount() {
        if(this.state.hives.length === 0) {
            get('hives')
            .then(val => {
                this.setState({hives: JSON.parse(val)})
            }).catch(err => console.log('DB Load failed', err));
            get('aufgaben')
            .then(val => {
                this.setState({aufgaben: JSON.parse(val)})
            }).catch(err => console.log('DB Load failed', err));
            get('kassenbuch-table')
            .then(val => {
                const temp = JSON.parse(val)
                this.setState({balance: temp[temp.length - 1].saldo})
                this.setState({lastEntrys: temp.slice(-3)})
            }).catch(err => console.log('DB Load failed', err));
        }
        
    }

    handleInputChange = (event) => {
        const target = event.target;
        const value = target.className === 'select' ? target.text : target.value;
        const name = target.name;
        this.setState({
          [name]: value    });
    }

    componentDidUpdate() {
        //update db
        set('aufgaben', JSON.stringify(this.state.aufgaben));
    }
}


 
export default Home;