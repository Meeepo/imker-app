import React, { Component } from 'react'
import { Button, Container, FormControl, InputGroup } from 'react-bootstrap';
import { set, get } from "idb-keyval";
import DatePicker, { registerLocale, setDefaultLocale } from  "react-datepicker";
import de from 'date-fns/locale/de';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import ToolkitProvider, { CSVExport } from 'react-bootstrap-table2-toolkit';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import "./css/react-datepicker.css";
registerLocale('de', de);
setDefaultLocale('de');
const { ExportCSVButton } = CSVExport;

class Finance extends Component {
    
    state = { date: new Date(),
              text: "",
              amount: "0",
              type: "Einnahmen",
              entrys: [],
              columns: [{
                dataField: 'id',
                text: 'Entry ID',
                sort: true
              }, {
                dataField: 'date',
                text: 'Datum'
              }, {
                dataField: 'text',
                text: 'Text'
              }, {
                dataField: 'type',
                text: 'Art'
              }, {
                dataField: 'amount',
                text: 'Wert'
              }, {
                dataField: 'saldo',
                text: 'Saldo'
              }]
    }

    componentDidMount() {
        get('kassenbuch-table')
            .then(val => this.setState({entrys: JSON.parse(val)}))
            .catch(err => console.log('DB Load failed', err));
      }

    handleInputChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
          [name]: value    });
    }

    handleSave = async () => {
        //calc saldo
        let saldo = 0
        if (typeof this.state.entrys[(this.state.entrys.length-1)] != 'undefined') {
            if(this.state.type === 'Einnahmen') {
                saldo = Number(this.state.entrys[(this.state.entrys.length-1)].saldo) + Number(this.state.amount)
            }
            if(this.state.type === 'Ausgaben') {
                saldo = Number(this.state.entrys[(this.state.entrys.length-1)].saldo) - Number(this.state.amount)
            }            
        } else {
          if(this.state.type === 'Einnahmen') { 
            saldo += Number(this.state.amount)
          }
          if(this.state.type === 'Ausgaben') { 
            saldo -= Number(this.state.amount)
          }
        }
        //update table
        await this.setState(entrys => ({
            entrys: [...this.state.entrys, {id: (this.state.entrys.length + 1), date: (this.state.date.toLocaleDateString(de)), text: this.state.text, type: this.state.type, amount: this.state.amount, saldo: saldo.toString()}]
          }))
        //update db
        set('kassenbuch-table', JSON.stringify(this.state.entrys));
        this.setState({text: ""})
    }

    handleClear = async () => {
      //update table
      await this.setState({entrys: []})
      set('kassenbuch-table', JSON.stringify(this.state.entrys));
    }

    render() { 
        return (
        <div>
          <AppBar position="static" style={{ background: '#db8300' }}>
                <Toolbar>
                    <Typography variant="h6" color="inherit">
                    Kassenbuch
                    </Typography>
                </Toolbar>
            </AppBar>
        <Container fluid>
        <InputGroup className="mb-3 mt-3">
            <InputGroup.Prepend>
                <InputGroup.Text>Buchungstext</InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl name="text" value={this.state.text} onChange={this.handleInputChange} aria-label="text">
            </FormControl>
        </InputGroup>
        <InputGroup className="mb-3">
            <InputGroup.Prepend>
                <InputGroup.Text>Wert</InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl name="amount" value={this.state.amount} onChange={this.handleInputChange} aria-label="amount" />
            <InputGroup.Append>
                <InputGroup.Text>€</InputGroup.Text>
            </InputGroup.Append>
        </InputGroup>
        <InputGroup className="mb-3">
            <InputGroup.Prepend>
                <InputGroup.Text>Buchungsart</InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl name="type" value={this.state.type} onChange={this.handleInputChange} as="select" aria-label="type">
            <option value="Einnahmen">Einnahmen</option>
            <option value="Ausgaben">Ausgaben</option>
            </FormControl>
        </InputGroup>
        <InputGroup className="mb-3 mt-3">
            <InputGroup.Prepend>
                <InputGroup.Text>Buchungdatum</InputGroup.Text>
            </InputGroup.Prepend>
            <DatePicker className="form-control" selected={this.state.date} onChange={cDate => this.setState({date: cDate})}/>
        </InputGroup>
        <Button onClick={this.handleSave} className="mb-3" >Speichern</Button>
        <Button onClick={this.handleClear} className="mb-3 ml-3" >Kassenbuch leeren</Button>
        <ToolkitProvider
          keyField="id"
          data={ this.state.entrys }
          columns={ this.state.columns }
          exportCSV
        >
          {
            props => (
              <div>
                <hr />
                <BootstrapTable { ...props.baseProps } pagination={ paginationFactory() } defaultSorted={[{dataField: 'id',order: 'desc'}]}/>
                <Button className="mt-3 mb-3" as="div"><ExportCSVButton { ...props.csvProps }>Export</ExportCSVButton></Button>
              </div>
            )
          }
        </ToolkitProvider>
        </Container>
      </div> );
    }

}
 
export default Finance;