import React, { PureComponent } from 'react'
import SideNav, { NavItem, NavIcon, NavText } from '@trendmicro/react-sidenav';
import './css/sidebar.css';
import { BsArchiveFill } from 'react-icons/bs'
import { FaHome,FaCashRegister, FaWikipediaW, FaMap } from 'react-icons/fa'
import { TiWeatherPartlySunny } from 'react-icons/ti'
import { BrowserRouter, Route, Redirect } from 'react-router-dom';
import styled from 'styled-components';

//import pages
import Home from '../components/home'; 
import Wiki from '../components/wiki';
import Hives from '../components/hives';
import Finance from '../components/finance';
import HiveMap from '../components/hivemap';
import Storage from '../components/storage';
import Weather from '../components/weather';

const Main = styled.main`
    position: relative;
    overflow: hidden;
    transition: all .15s;
    padding: 0 20px;
    margin-left: ${props => (props.expanded ? 240 : 64)}px;
`;


class Sidebar extends PureComponent {
    state = { 
        selected: 'home',
        expanded: false
     }    

    navigate = (pathname) => () => {
        this.setState({ selected: pathname });
    };

    render() { 
        const { expanded } = this.state;
        return ( 
            <BrowserRouter>
            <Route render={({ location, history }) => (
                <React.Fragment>
                    <SideNav
                        onToggle ={(expanded) => {
                            this.setState({ expanded: expanded });
                        }}
                        onSelect={(selected) => {
                            const to = '/' + selected;
                            if (location.pathname !== to) {
                                history.push(to);
                            }
                        }}
                    >
                        <SideNav.Toggle />
                        <SideNav.Nav defaultSelected="home">
                            <NavItem eventKey="home">
                                <NavIcon>
                                    <FaHome />
                                </NavIcon>
                                <NavText>
                                    Home
                                </NavText>
                            </NavItem>
                            <NavItem eventKey="wiki">
                                <NavIcon>
                                    <FaWikipediaW />
                                </NavIcon>
                                <NavText>
                                Bienenlexikon
                                </NavText>
                            </NavItem>
                            <NavItem eventKey="hives">
                                <NavIcon>
                                    <BsArchiveFill />
                                </NavIcon>
                                <NavText>
                                    Bienenstöcke
                                </NavText>
                            </NavItem>
                            <NavItem eventKey="finance">
                                <NavIcon>
                                    <FaCashRegister />
                                </NavIcon>
                                <NavText>
                                    Kassenbuch
                                </NavText>
                            </NavItem>
                            {/* 
                            <NavItem eventKey="storage">
                                <NavIcon>
                                    <AiFillAppstore />
                                </NavIcon>
                                <NavText>
                                    Lagerverwaltung
                                </NavText>
                            </NavItem>*/}
                            <NavItem eventKey="hivemap">
                                <NavIcon>
                                    <FaMap />
                                </NavIcon>
                                <NavText>
                                    Karte
                                </NavText>
                            </NavItem>
                            <NavItem eventKey="weather">
                                <NavIcon>
                                    <TiWeatherPartlySunny />
                                </NavIcon>
                                <NavText>
                                    Wetter
                                </NavText>
                            </NavItem>     
                        </SideNav.Nav>
                    </SideNav>
                    <Main expanded={expanded}>
                        <Route path="/" exact component={props => <Home />} />
                        <Route path="/home" component={props => <Home />} />
                        <Route path="/wiki" component={props => <Wiki />} />
                        <Route path="/finance" component={props => <Finance />} />
                        <Route path="/hivemap" component={props => <HiveMap />} />
                        <Route path="/hives" component={props => <Hives />} />
                        <Route path="/storage" component={props => <Storage />} />
                        <Route path="/weather" component={props => <Weather />} />
                        <Redirect to="/" />
                    </Main>
                </React.Fragment>
            )}
            />
        </BrowserRouter>
        );
    }
}
 
export default Sidebar;