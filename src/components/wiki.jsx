import React, { Component } from 'react'
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import {Button,Accordion, Card, Container } from 'react-bootstrap'

class Wiki extends Component {
    state = {  }
    render() { 
        return (
            
                <div> 
                    <AppBar position="static" style={{ background: '#db8300' }}>
                        <Toolbar>
                            <Typography variant="h6" color="inherit">
                            Bienenlexikon
                            </Typography>
                        </Toolbar>
                    </AppBar>
                    <Container fluid>
                            
                            <Accordion className="mt-3 mb-3">
                                <Card>
                                    <Card.Header>
                                        <Accordion.Toggle as={Button} variant="link" eventKey="0">
                                            Beschreibung
                                        </Accordion.Toggle>
                                    </Card.Header>
                                        <Accordion.Collapse eventKey="0">
                                            <Card.Body>
                                            <p>Die Bienen (Apiformes oder Anthophila) sind eine Insektengruppe, in der mehrere Familien der Hautflügler (Hymenoptera) zusammengefasst werden.</p>
                                            <p>Umgangssprachlich wird der Begriff Biene meist auf eine einzelne Art, die Westliche Honigbiene (Apis mellifera), reduziert, die wegen ihrer Bedeutung als staatenbildender Honigproduzent, aber auch wegen ihrer Wehrhaftigkeit besondere Aufmerksamkeit erfährt. Dabei handelt es sich bei den Bienen um eine recht große Gruppe mit sehr unterschiedlichen Arten. Viele davon, vor allem die solitär lebenden, werden unter dem Begriff Wildbienen zusammengefasst. </p>
                                            <p>Der wissenschaftliche Name der Bienen wird, je nach Autoren, unterschiedlich angegeben. Charles Michener bevorzugt in seinem Standardwerk The Bees of the World den Namen Apiformes. Er folgt darin dem Entomologen Denis J. Brothers. Zahlreiche andere Autoren nennen die Bienen Anthophila. Dieser Name, der bereits im 19. Jahrhundert verwendet wurde, wurde durch Michael S. Engel reaktiviert.</p>
                                            </Card.Body>
                                        </Accordion.Collapse>
                                </Card>
                                <Card>
                                    <Card.Header>
                                        <Accordion.Toggle as={Button} variant="link" eventKey="1">
                                            Morphologie
                                        </Accordion.Toggle>
                                    </Card.Header>
                                        <Accordion.Collapse eventKey="1">
                                            <Card.Body>
                                            <p>In der Regel sind Bienen ca. 10 mm lang, die kleinsten sind aber nur 1,5 mm lang, die größte Biene, Megachile pluto gilt als größte Biene mit über 40 mm Länge.</p>
                                            <p>Bienen haben die insektentypische Dreiteilung des Körpers mit vier Flügeln, die Wespentaille wie alle Aculeata, sowie einen Wehrstachel. Da sich dieser phylogenetisch aus dem Legebohrer entwickelt hat, haben nur Weibchen einen Stachel.</p>
                                            <p>Ein besonderes Merkmal ist die Behaarung der Bienen, wobei stets mindestens einige Haare gefiedert sind. Häufig sind die Hinterbeine (Beinsammler) oder die Unterseite des Hinterleibs (Bauchsammler) besonders stark behaart. Vielfach dient die Behaarung zum Pollentransport. Gerade an den verzweigten Härchen kann Pollen hängen bleiben. Durch den Besuch mehrerer Blüten trägt die Biene maßgeblich zur Bestäubung bei. Bei Kuckucksbienen ist die Behaarung oft stark reduziert, aber fast immer am Propodeum noch erhalten. Der Basitarsus der Hinterbeine ist sowohl bei Männchen als auch bei Weibchen abgeflacht (im Gegensatz zu den Grabwespen). An den Füßen haben sie meist Krallen und Haftlappen.</p>
                                            <p>Die Antennen sind gekniet (d. h. das erste Antennenglied ist deutlich länger als die folgenden). Die Männchen haben 13, die Weibchen 12 Antennenglieder (Ausnahme Pasites und Biastes). Die Mundwerkzeuge sind zu einem Rüssel umgestaltet, die Mandibeln (Oberkiefer) sind jedoch Beißorgane.</p>
                                            </Card.Body>
                                        </Accordion.Collapse>
                                </Card>
                                <Card>
                                    <Card.Header>
                                        <Accordion.Toggle as={Button} variant="link" eventKey="2">
                                        Lebensweise
                                        </Accordion.Toggle>
                                    </Card.Header>
                                    <Accordion.Collapse eventKey="2">
                                        <Card.Body>
                                        <h3>Ernährung</h3>
                                        <p>Bienen ernähren sich rein vegetarisch. Ihre wichtigste Nahrungsquelle sind süße Pflanzensäfte – insbesondere Nektar. Für die Eiweißversorgung sind sie auf Pollen angewiesen. Arten der staatenbildenden Gattungen und Familien, wie Hummeln, Meliponini und vor allem Honigbienen legen Futtervorräte an. Dies dient zum Überleben als ganze Kolonie in nahrungsfreien Zeiten, wie zum Beispiel der Regenzeit (Tropen) oder einer Winterperiode in kühleren Klimaregionen.</p>
                                        <p>Polylektische Bienenarten sammeln Pollen als Nahrung für ihre Brut an Trachtpflanzenarten aus verschiedenen botanischen Familien. Im Gegensatz zu oligolektischen Arten handelt es sich um ökologische Generalisten.</p>
                                        <p>Oligolektische Bienenarten sammeln Pollen als Nahrung für ihre Brut ausschließlich von Pflanzenarten einer Familie. Da sie im Gegensatz zu polylektischen Arten nicht in der Lage sind, auf andere Larvenfutterpflanzen auszuweichen, sind sie beim Verschwinden ihrer Pollenspender lokal vom Aussterben bedroht. Die extreme Form der Oligolektie ist die Monolektie.</p>
                                        <p>Als Monolektie wird das Verhalten von Bienenarten bezeichnet, die Pollen als Nahrung für ihre Brut ausschließlich von Pflanzenarten einer Gattung sammeln. Monolektische Arten sind damit zur Fortpflanzung völlig auf das Vorkommen ihrer artspezifischen Larvenfutterpflanzen angewiesen.</p>
                                        <p>Eine Besonderheit stellen Bienen dar, deren Weibchen Öl in Blüten sammeln, entweder als Nahrung für die Larven oder sich selbst oder für den Nestbau. Bekannt dafür sind vor allem die Gattungen Macropis und Ctenoplectra.</p>
                                        <h3>Sozialverbände und Staaten</h3>
                                        <p>Hochsoziale Gemeinschaftsformen, insbesondere Staaten wie bei der Honigbiene, sind unter den Bienenarten die Ausnahme. Solche Gemeinschaften konzentrieren sich fast ausschließlich auf die Körbchensammler innerhalb der Familie der Apidae, nämlich auf Apis mit 9 Arten, Bombus mit rund 250 und Meliponini (Stachellose Bienen) mit rund 370 Arten.</p>
                                        <p>Die überwältigende Mehrzahl aller Bienenarten sind Solitärbienen (ca. 75 %) und Kuckucksbienen (ca. 15 %), die keine Insektenstaaten bilden, sondern allein leben und nur für die eigene Nachkommenschaft Brutpflege betreiben. Das Ei wird bei Solitärbienen zusammen mit einem Nahrungsvorrat abgelegt und der Ablageplatz fest verschlossen. Kuckucksbienen sind Brutschmarotzer, die ihre Eier in die Brutzellen anderer Bienenarten legen, wo diese die Larve töten und sich vom Nahrungsvorrat ernähren. Dieses Verhalten wird allgemein als Sozialparasitismus bezeichnet. Etwa 10 % der Bienen sind sozial.</p>
                                        <p>Zwischen solitärer Lebensweise und der Staatenbildung gibt es eine ganze Skala von Zwischenformen:</p>
                                        <ul>
                                            <li>Gemeinschaftliches Abwehrverhalten bei größeren Ansammlungen von Bienen, die ansonsten in unabhängiger Nachbarschaft nebeneinanderher leben. Beispielsweise wurden bei der Weiden-Sandbiene (Andrena vaga) und der Gemeinen Pelzbiene (Anthophora plumipes) Schwarm-Angriffe auf Menschen beobachtet, die in den Flugbereich einer Kolonie gerieten.</li>
                                            <li>Überwinterungsgemeinschaften in gemeinschaftlich genutzten Erdhöhlen oder Pflanzen-Aushöhlungen.</li>
                                            <li>Schlafgemeinschaften von Bienenmännchen im Frühjahr. Diese Schlafgemeinschaften finden sich meist an exponierten Stellen zusammen, insbesondere an der Spitze von Pflanzenstängeln. Der biologische Sinn dieser Gemeinschaften ist noch unklar, da die Tiere in ihnen weder Nahrung noch Schutz vor Feinden, Kälte oder Wind finden.</li>
                                            <li>Nistgemeinschaften mit gemeinsamen Nesteingängen. Innerhalb der Nistgemeinschaft besetzt jedes Weibchen eine eigene Zelle, in der es ihr eigenes Ei ablegt. Bei Gedränge am Eingang nehmen die Weibchen aufeinander Rücksicht.</li>
                                            <li>Wachdienste am Eingang der Nistgemeinschaften.</li>
                                            <li>Zusammenarbeit bei der Anlage und der Verproviantierung der Zellen.</li>
                                            <li>Arbeitsteilung bei der Fortpflanzung: Nur ein Teil der Weibchen legt Eier, die anderen kümmern sich um Nestbau, Proviant und Wachdienst.</li>
                                            <li>Brutpflege durch Nachfütterung der Larven und Beiseiteschaffen von deren Kot.</li>
                                            <li>Weitere Spezialisierung bei der Fortpflanzung. Bei der Furchenbiene Laxioglossum pauxillum beispielsweise baut das überwinterte Weibchen im Frühjahr einen Nestgang mit bis zu 25 Zellen, in das sie ihre Eier legt. Die Nachkommen pflanzen sich nicht fort, sondern erweitern das Nest und pflegen die weitere Nachkommenschaft ihrer Mutter. Erst im Spätsommer werden die Drohnen, so heißen bei den staatenbildenden Bienen und Faltenwespen die Männchen, und größere, fortpflanzungsfähige Weibchen geboren. Die Mutter stirbt, und die begatteten Jungweibchen gründen im nächsten Frühjahr neue Kolonien. Dieses Fortpflanzungs- und Brutpflegeverhalten kommt den Verhältnissen in einem Bienenstaat schon recht nahe.</li>
                                        </ul>
                                        <p></p>
                                        </Card.Body>
                                    </Accordion.Collapse>
                                </Card>
                                <Card>
                                    <Card.Header>
                                        <Accordion.Toggle as={Button} variant="link" eventKey="3">
                                        Ökologie, Ökonomie
                                        </Accordion.Toggle>
                                    </Card.Header>
                                    <Accordion.Collapse eventKey="3">
                                        <Card.Body>
                                            <p>Da Bienen in erheblichem Maße zur Erhaltung von Wild- und Kulturpflanzen und deren Erträgen beitragen, ist ihre ökologische Bedeutung beträchtlich; Bienen zählen weltweit zu den wichtigsten Bestäubern. Nach der Umweltschutzorganisation Greenpeace liegt der Gegenwert ihrer jährlichen Bestäubungsleistung weltweit bei rund 265 Milliarden Dollar. Ihre damit zusammenhängende ökonomische Bedeutung wird auch dadurch deutlich, dass zum Beispiel in Deutschland derzeit von über 80.000 Imkern zirka eine Million Bienenvölker gehalten werden. Diese decken mit etwa 25.000 Tonnen Honig pro Jahr etwa 20 % des heimischen Bedarfs.</p>
                                            <p>Seit einigen Jahren wird ein zunehmendes sogenanntes „Bienensterben“ beobachtet. Als Messgröße der Imkerei bezieht sich der Ausdruck nicht auf den Tod einzelner Bienen, sondern auf die Völkerverluste der Honigbiene. Dabei verbergen sich hinter dem Schlagwort ganz unterschiedliche Phänomene: etwa das Verschwinden ganzer Bienenvölker mitten in der Saison, insbesondere in den USA („Colony Collapse Disorder“), oder aber ungewöhnlich hohe Winterverluste (so zum Beispiel in Deutschland im Winter 2002/2003).</p>                                        
                                            <p>Mitte Dezember 2017 erklärte die in New York tagende Generalversammlung der Vereinten Nationen auf Vorschlag von Slowenien mit Unterstützung aller EU-Staaten den 20. Mai zum „Welttag der Bienen“: Dieser soll „durch Bildung und Aktivitäten das Bewusstsein für die Wichtigkeit von Bienen und anderen Bestäubern, die Gefahren, denen sie ausgesetzt sind, sowie ihren Beitrag für eine nachhaltige Entwicklung erhöht werden“.</p>
                                        </Card.Body>
                                    </Accordion.Collapse>
                                </Card>
                                <Card>
                                    <Card.Header>
                                        <Accordion.Toggle as={Button} variant="link" eventKey="4">
                                            Systematik und Evolution
                                        </Accordion.Toggle>
                                    </Card.Header>
                                    <Accordion.Collapse eventKey="4">
                                        <Card.Body>
                                        <p>Weltweit wird die Zahl der Bienenarten auf rund 20.250 geschätzt. Davon sind in Europa etwa 700 Arten heimisch, davon wiederum etwa 500 in Deutschland. Besonders artenreiche Gattungen sind Lasioglossum, Andrena und Megachile mit jeweils mehr als 1.500 Arten. Die über einen Wehrstachel verfügenden Bienen gehören zu den Stechimmen.</p>
                                        <h3>Entwicklungsgeschichte</h3>
                                        <p>Heutige Bienen sind auf Blütenpflanzen, die Bedecktsamer (Angiospermen), angewiesen, die in der Erdgeschichte in der frühen Kreidezeit auftauchten und seit der späten Kreidezeit die Nacktsamer und Gefäßsporenpflanzen verdrängten. Blütenpflanzen aus der Zeit vor etwa 110 Millionen Jahren weisen bereits Merkmale auf, die auf eine Bestäubung durch Bienen schließen lassen, der Ursprung der Bienen liegt damit wahrscheinlich schon vor Mitte der Kreidezeit. Möglicherweise waren diese Pflanzen schon früher verbreitet, lassen sich aber durch die geringeren Mengen produzierten Pollens nicht nachweisen.</p>
                                        <p>Die heutigen ursprünglichsten Blütenpflanzen werden von Käfern bestäubt, es liegt daher nahe, diese auch als Bestäuber der ersten kreidezeitlichen Blütenpflanzen zu vermuten. Im weiteren Verlauf der Stammesgeschichte haben sich aber Bienen und Blütenpflanzen gemeinschaftlich entwickelt und gegenseitig gefördert: Indem Bienen die Pollen von Pflanze zu Pflanze weiter trugen, verbesserten sie deren Fortpflanzungschancen. Die Pflanzen begannen sich darauf einzustellen und entwickelten süße Säfte, um die Tiere an sich zu binden. Mit der Zeit passten sich beide, Bienen und Blütenpflanzen, immer besser aneinander an (Ko-Evolution): die Pflanzen entwickelten ihre heutigen Blütenformen mit tiefen Nektarkelchen und Staubfäden, die Bienen ihre langen Rüssel, um gut an den Nektar heranzukommen, und ihr speziell an den Pollentransport angepasstes Haarkleid.</p>
                                        <p>Ob Bienen sich ursprünglich von Pollen windbestäubter Pflanzen ernährten, ist ungewiss, aber schon mehrfach vermutet worden.</p>
                                        <p>Die älteste fossile Biene ist als Cretotrigona prisca bezeichnet und wurde – eingebettet in Bernstein – im amerikanischen Staat New Jersey gefunden. Der Fund ist auf ein Alter von ca. 75 bis 92 Millionen Jahren datiert. Bemerkenswert ist, dass das Tier in eine Tribus (Meliponini) eingegliedert werden kann, die ausschließlich staatenbildende Arten enthält, was auf eine sehr frühe Abspaltung der entsprechenden Teilgruppe schließen lässt. Ursprünglich wurde sie sogar in einer noch lebenden Gattung beschrieben.</p>
                                        <p>Stammesgeschichtliche Vorläufer der Bienen dürften heutigen Grabwespen geähnelt haben. Grabwespen versorgen ihre Brut mit einem Nahrungsvorrat, indem sie ein Beutetier mit einem Stich lähmen und dann gemeinsam mit ihrem Ei oder ihren Eiern vergraben. Dieses Brutverhalten ähnelt dem der heutigen Solitärbienen, mit dem Unterschied, dass letztere kein Beutetier, sondern Pollen als Nahrungsvorrat für ihren Nachwuchs verwenden. Es ist anzunehmen, dass die Bienen eine Schwestergruppe einer Teilgruppe der Crabronidae (Ammoplanina) sein dürften.</p>
                                        <h3>Phylogenetische Systematik</h3>
                                        <p>Der monophyletische Status der Bienen ist durch zahlreiche gemeinsame abgeleitete Merkmale (Synapomorphien) belegt und gilt als unbestritten. Ebenso unbestritten ist die nahe Verwandtschaft der Bienen mit den Grabwespen. Lange Zeit betrachtete man beide Gruppen als Schwestergruppen, bis die Grabwespen als paraphyletisch erkannt wurden. Eine Zusammenfassung der Grabwespen in einer Familie (Sphecidae) oder einer Überfamilie (Sphecoidea), die den Bienen in Form der Überfamilie Apoidea gleichrangig gegenübergestellt wird, gibt die tatsächlichen systematischen Verhältnisse nur unbefriedigend wieder. Passender wäre die Eingliederung der Bienen als einzige Familie unter mehreren Familien der Grabwespen.</p>
                                        <p>Andererseits hat sich innerhalb der Bienen die Unterscheidung mehrerer Familien längst etabliert. Durch deutliche Unterschiede im Körperbau erscheint diese gerechtfertigt und ist Basis weiterer Unterteilungen auf unteren taxonomischen Ebenen. Ein Kompromiss, der von den führenden Systematikern der Gruppe vorgeschlagen wird, fasst unter der Überfamilie Apoidea die Familien der Grabwespen und Bienen in zwei Serien (Spheciformes und Apiformes) zusammen.</p>
                                        <h3>Familien der Bienen</h3>
                                        <p>Die Unterteilung der Bienen in mehrere Familien basiert unter anderem auf dem Bau der Mundwerkzeuge, ein wichtiges Merkmal ist etwa die Unterscheidung von kurz- und langzüngigen Bienen. Nach Plant & Paulus (2006) werden die Bienen in folgende Familien und Unterfamilien untergliedert:</p>
                                        <h6>Klade I: Kurzzungige Bienen</h6>
                                        <ul>
                                            <li>Halictidae: etwa 4.400 Arten, knapp 80 Gattungen, Unterfamilien: Halictinae, Nomioidinae, Nomiinae, Rophitinae (=Dufoureinae); Gattungen in Mitteleuropa (nach): Furchenbienen (Halictus und Lasioglossum), Spiralhornbienen, Dufourea, Rhophitoides, Rophites, Nomia, Nomioides, Sphecodes</li>
                                            <li>Andrenidae: knapp 3.000 Arten, 46 Gattungen, Unterfamilien: Andreninae, Panurginae, Oxaeinae (teilweise als eigene Familie betrachtet); Gattungen in Mitteleuropa die Sandbienen, Zottelbienen, Camptopoeum, Panurginus, Melitturga</li>
                                            <li>Stenotritidae: 21 Arten, 2 Gattungen, auf Australien beschränkt (werden teilweise zu den Colletidae gestellt)</li>
                                            <li>Colletidae: ca. 2.500 Arten, 90 Gattungen, zum Großteil Kropfsammler, Unterfamilien: Diphaglossinae, Colletinae; In Mitteleuropa vertreten durch die Seidenbienen und Maskenbienen</li>
                                        </ul>
                                        <h6>Klade II (Melittidae + Langzungige Bienen)</h6>
                                        <li>Melittidae: etwa 200 Arten, 14 Gattungen, Unterfamilien: Dasipodainae, Melittinae, Meganomiinae; Galten als „kurzzungig“, sind aber die Schwestergruppe von (Megachilidae + Apidae).[6] in Mitteleuropa: Hosenbienen, Schenkelbienen und Sägehornbienen</li>
                                        <h6>Langzungige Bienen:</h6>
                                        <ul>
                                            <li>Megachilidae: ca. 2.135 Arten, 76 Gattungen, Unterfamilien: Pararhophitinae, Fideliinae, Lithurginae, Megachilinae; Bauchsammler, in Mitteleuropa die Gattungen Osmia, Hoplitis, Hoplosmia (Mauerbienen), Anthidium, Lithurgus, Stelis, Dioxys, Megachile, Coelioxys, Chelostoma, Heriades</li>
                                            <li>Apidae: ca. 6.035 Arten, 172 Gattungen, Unterfamilien: Nomadinae, Xylocopinae, Apinae; sehr unterschiedlicher Formen, in Mitteleuropa die folgenden Tribus und Gattungen:
                                                <ul>
                                                    <li>Tribus Nomadini: Ammobates, Pasites, Ammobatoides, Biastes, Epeolus, Triepeolus und Nomada</li>
                                                    <li>Tribus Xylocopini: Holzbienen (Xylocopa) und Ceratina</li>
                                                    <li>Tribus Apini: Epeoloides, Eucera, Tetralonia, Anthophora, Melecta, Thyreus, Hummeln, Kuckuckshummeln sowie die Honigbienen (Apis). In der Neotropis z. B. Meliponini (unter anderem mit Melipona und Trigona) und Euglossini (Euglossa, Eulaema, Eufriesea, Exaerete und Aglae).</li>
                                                </ul>
                                            </li>
                                        </ul>
                                        </Card.Body>
                                    </Accordion.Collapse>
                                </Card>
                                <Card>
                                    <Card.Header>
                                        <Accordion.Toggle as={Button} variant="link" eventKey="5">
                                            Der Stachel
                                        </Accordion.Toggle>
                                    </Card.Header>
                                    <Accordion.Collapse eventKey="5">
                                        <Card.Body>
                                            <p>Der Giftstachel der Bienen ist ein Wehrstachel. Er hat sich im Laufe der Entwicklungsgeschichte (Evolution) aus einem Eilegeapparat gebildet. Folglich haben stets nur die Weibchen, also Königinnen und Arbeiterinnen, einen Stachel. Dies gilt für alle Stechimmen. Männchen können also grundsätzlich nicht stechen.</p>
                                            <p>Die Pflanzenwespen benutzen den Stachel zum Anstechen von Pflanzen für die Eiablage; bei den Legimmen, wie etwa den Schlupfwespen, wird das Ei im Körper eines Wirtstieres abgelegt. Bei den Stechimmen wandelte sich die Funktion des Stachels; er dient zur Verabreichung von Gift, um Beutetiere zu lähmen, die als Larvennahrung dienen. Schließlich hat bei den Bienen, die ihre Larven überwiegend mit Blütenpollen versorgen, der Stachel eine reine Verteidigungsfunktion. Besondere Bedeutung hat dabei die Verteidigung des Staates bei den staatenbildenden Bienen, den Honigbienen und den Hummeln. Da der Stachel der Honigbienen mit Widerhaken ausgestattet ist, bleibt er beim Stich in die elastische Haut des Menschen und anderer Warmblüter stecken und die Biene stirbt. Die bei einem Stich injizierte Giftmenge wird mit 0,1 mg angegeben.</p>
                                            <p>Bei den meisten Wildbienen wird der Stachel nur eingesetzt, wenn sich die Biene individuell bedroht fühlt, z. B. wenn sie zwischen Fingern gedrückt wird. Meistens ist bei Stichen von Wildbienen der Schmerz nicht sehr stark und damit harmlos. Lediglich wenn man allergisch ist, was aber bei Wildbienen sehr selten ist, besteht eine echte Gefahr. Bei Maskenbienen und Sandbienen ist der Stachel so schwach, dass er die menschliche Haut gar nicht durchdringen kann. In den Tropen gibt es stachellose Bienen (Meliponini), die sich mit Beißen und Sekreten wehren können.</p>
                                        </Card.Body>
                                    </Accordion.Collapse>
                                </Card>
                                <Card>
                                    <Card.Header>
                                        <Accordion.Toggle as={Button} variant="link" eventKey="6">
                                            Politik
                                        </Accordion.Toggle>
                                    </Card.Header>
                                    <Accordion.Collapse eventKey="6">
                                        <Card.Body>
                                            <p>„Rettet die Bienen“ ist ein Volksbegehren in Bayern, das Artenvielfalt fördern und das Insektensterben aufhalten will. 18,4 % der stimmberechtigten Bayern haben dafür ihre Unterschrift abgegeben. Der Maßnahmenkatalog beinhaltet einschneidende Veränderungen. In Brandenburg sind gleich zwei konkurrierende Volksinitiativen zum Thema gestartet, eine im Dezember 2019, eine im Februar 2020, dabei wird eine der Initiativen eher von den Umweltverbänden unterstützt, die zweite, weniger strenge von den Landnutzern. Der Brandenburger Landtag erklärte allerdings im März 2020 eine der Initiativen für unzulässig. Auch das Bundesumweltministerium will ein deutschlandweites Insektenschutzgesetz beschließen lassen.</p>
                                        </Card.Body>
                                    </Accordion.Collapse>
                                </Card>
                            </Accordion>                            
                    </Container>
                </div>
         );
    }
}

  
 
export default Wiki;