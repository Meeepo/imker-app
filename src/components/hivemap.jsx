import React, { Component } from 'react';
import { get, set } from "idb-keyval";
import { Container, Alert } from 'react-bootstrap'
import { geolocated, geoPropTypes } from "react-geolocated";
import Map from "./util/map/map";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";



class HiveMap extends Component {

    state = { hives: [],
            coords: [51, 10],
            updateLocation: true
            }

    handleInputChange = async (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        await this.setState({
            [name]: value    });
        set('map-updateLocation', JSON.stringify(this.state.updateLocation));
    }
    
    render() { 
        return ( <div>
            <AppBar position="static" style={{ background: '#db8300' }}>
                <Toolbar>
                    <Typography variant="h6" color="inherit">
                        Karte
                    </Typography>
                </Toolbar>
            </AppBar>
            <Container fluid className="mt-3">
            <Alert className="mt-3 mr-3 ml-3" variant="dark"><div className="mt-3 mb-3 form-check">
                <input name="updateLocation" value={this.state.updateLocation} onChange={this.handleInputChange} type="checkbox" className="form-check-input" id="updateLocation" checked={this.state.updateLocation}/>
                <label className="form-check-label" htmlFor="updateLocation">Aktualisiere Standort</label>
            </div>
            </Alert>
            <Map coords={this.state.coords}
                 hives={this.state.hives}
            />
            </Container>
        </div> );
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return nextProps.coords === null || !prevState.updateLocation || nextProps.coord === prevState.coords
        ? {}
        : {coords: [nextProps.coords.latitude, nextProps.coords.longitude]}
    }      

    componentDidMount() {
        get('hives')
            .then(val => {
                this.setState({hives: JSON.parse(val)})
            }).catch(err => console.log('DB Load failed', err));
        get('map-updateLocation')
            .then(val => {
                this.setState({updateLocation: JSON.parse(val)})
            }).catch(err => console.log('DB Load failed', err));
    }    
}

// Using Object.assign
HiveMap.propTypes = Object.assign({}, HiveMap.propTypes, geoPropTypes);
// Using ES6 object spread syntax
HiveMap.propTypes = { ...HiveMap.propTypes, ...geoPropTypes };
 
export default geolocated({
    positionOptions: {
        enableHighAccuracy: true,
    },
    userDecisionTimeout: 5000,
}) (HiveMap);