import React from 'react';
import Sidebar from './components/sidebar';
import '@forevolve/bootstrap-dark/dist/css/bootstrap-dark.min.css';

function App() {
  return (
    <div>
      <Sidebar />
      <script src="/__/firebase/8.1.1/firebase-app.js"></script>
      <script src="/__/firebase/8.1.1/firebase-analytics.js"></script>
      <script src="/__/firebase/init.js"></script>   
    </div>
      
  );
}

export default App;
